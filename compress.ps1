# Script that compresses a powershell script and converts it to a self-decompressing powershell script
param([string]$in = "in.ps1", [string]$out = "out.ps1")

$content = [IO.File]::ReadAllText($in)

$ms = New-Object System.IO.MemoryStream
$cs = New-Object System.IO.Compression.GZipStream($ms, [System.IO.Compression.CompressionMode]::Compress)

$sw = New-Object System.IO.StreamWriter($cs)
$sw.Write($content)
$sw.Close();

$bytes = $ms.ToArray()
$output = '$d=[System.Convert]::FromBase64String("' + [System.Convert]::ToBase64String($bytes);
$output += '");$ms=New-Object System.IO.MemoryStream;$ms.Write($d,0,$d.Length);$ms.Seek(0,0)|Out-Null;$cs=New-Object System.IO.Compression.GZipStream($ms,[System.IO.Compression.CompressionMode]::Decompress);$sr=New-Object System.IO.StreamReader($cs);$t=$sr.readtoend();iex $t'

$output > $out